package com.pucrs.mock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by tkroth on 27/11/16.
 */
@RestController
@SpringBootApplication(scanBasePackages = "com.pucrs.mock")
public class Application {

    @Autowired
    MockService mockService;

    @RequestMapping(value = "mock/{year}/{month}", method = RequestMethod.GET)
    public Long getVarMock(@PathVariable int year, @PathVariable int month) {
        return mockService.getValue(year, month);
    }


    @RequestMapping(value = "temp/{year}/{month}", method = RequestMethod.GET)
    public Long getVarMock2(@PathVariable int year, @PathVariable int month) {
        return mockService.getValue(year, month);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }
}
