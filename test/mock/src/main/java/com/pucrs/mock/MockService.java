package com.pucrs.mock;

import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * Created by tkroth on 27/11/16.
 */
@Component
public class MockService {

    private static final String YEAR_PATTERN = "%s/%s";

    private HashMap<String, Long> init() {
        HashMap<String, Long> initialValue = new HashMap<>();

        for (int y = 2010; y < 2017; y++) {
            for (int m = 1; m < 13; m++) {
                double v = Math.random() * 10000;
                v = Math.floor(v*100)/100;
                initialValue.put(String.format(YEAR_PATTERN, m, y), (long) v);
            }
        }

        return initialValue;
    }

    public Long getValue(int year, int month) {
        HashMap<String, Long> init = init();
        return init.get(String.format(YEAR_PATTERN, month, year));
    }

}