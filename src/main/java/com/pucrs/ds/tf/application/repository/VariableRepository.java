package com.pucrs.ds.tf.application.repository;

import com.pucrs.ds.tf.domain.Variable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by tkroth on 05/10/16.
 */
public interface VariableRepository extends CrudRepository<Variable, Long> {

    List<Variable> findByClientId(Long clientId);

}
