package com.pucrs.ds.tf.application.impl;

import com.pucrs.ds.tf.application.ClientService;
import com.pucrs.ds.tf.application.VariableService;
import com.pucrs.ds.tf.application.repository.VariableRepository;
import com.pucrs.ds.tf.domain.Client;
import com.pucrs.ds.tf.domain.Variable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.security.InvalidParameterException;
import java.util.List;

/**
 * Created by tkroth on 05/10/16.
 */
@Component
public class VariableServiceImpl implements VariableService {

    @Autowired
    VariableRepository variableRepository;

    @Autowired
    ClientService clientService;

    @Override
    public Variable create(Long clientId, Variable variable) {
        Client client = clientService.get(clientId);
        if(client == null) {
            throw new InvalidParameterException("Client does not exist");
        }

        variable.setClient(client);
        return variableRepository.save(variable);
    }

    @Override
    public Variable get(Long id) {
        return variableRepository.findOne(id);
    }

    @Override
    public List<Variable> list(Long clientId) {
        return variableRepository.findByClientId(clientId);
    }
}
