package com.pucrs.ds.tf.application;

import com.pucrs.ds.tf.domain.Client;
import org.springframework.stereotype.Component;

/**
 * Created by tkroth on 05/10/16.
 */
@Component
public interface ClientService {

    Client create(Client client);

    Client get(Long id);
}
