package com.pucrs.ds.tf.application.repository;

import com.pucrs.ds.tf.domain.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by tkroth on 12/10/16.
 */
public interface UserRepository extends CrudRepository<User, Long> {

    User findByEmailAndPassword(String username, String password);
}
