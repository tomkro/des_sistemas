package com.pucrs.ds.tf.application;

import com.pucrs.ds.tf.domain.Indicator;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.List;

/**
 * Created by tkroth on 18/10/16.
 */
@Component
public interface IndicatorService {

    Indicator create(Long clientId, Indicator indicator);

    Indicator get(Long id);

    Map<String, Double> processIndicator(Long id, int iniYear, int iniMonth, int endYear, int endMonth);

    List<Indicator> list(Long clientId);
}
