package com.pucrs.ds.tf.application.impl;

import com.pucrs.ds.tf.application.ClientService;
import com.pucrs.ds.tf.application.IndicatorService;
import com.pucrs.ds.tf.application.VariableService;
import com.pucrs.ds.tf.application.repository.IndicatorRepository;
import com.pucrs.ds.tf.domain.Client;
import com.pucrs.ds.tf.domain.Indicator;
import com.pucrs.ds.tf.domain.Variable;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Created by tkroth on 18/10/16.
 */
@Component
public class IndicatorServiceImpl implements IndicatorService {

    private static final String VAR_PATTERN = "${%s}";
    private static final String YEAR_PATTERN = "%s/%s";


    @Autowired
    private IndicatorRepository indicatorRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private VariableService variableService;

    @Override
    public Indicator create(Long clientId, Indicator indicator) {
        Client client = clientService.get(clientId);

        if (ObjectUtils.isEmpty(client)) {
            throw new InvalidParameterException("Cliente informado não existe");
        }

        indicator.setClient(client);

        List<Variable> variables = indicator.getVariables().stream().map(var -> variableService.get(var.getId())).collect(Collectors.toList());
        indicator.setVariables(variables);
        return indicatorRepository.save(indicator);

    }

    @Override
    public Indicator get(Long id) {
        return indicatorRepository.findOne(id);
    }

    @Override
    public List<Indicator> list(Long clientId) {
        return indicatorRepository.findByClientId(clientId);
    }

    @Override
    public Map<String, Double> processIndicator(Long id, int iniYear, int iniMonth, int endYear, int endMonth) {

        Indicator indicator = indicatorRepository.findOne(id);

        if (indicator == null) {
            throw new InvalidParameterException("Indicador não existe");
        }

        Map<String, Double> result = new TreeMap<>();

        if (iniYear > endYear) {
            throw new InvalidParameterException("Anos informados são inválidos");
        }

        //String formula = indicator.getFormula();
        List<Variable> variables = indicator.getVariables();


        int year = iniYear;

        do {
            int month;
            int finalMonth;

            if (year < endYear) {
                month = year == iniYear ? iniMonth : 1;
                finalMonth = 12;
            } else if (iniYear == endYear) {
                month = iniMonth;
                finalMonth = endMonth;
            } else {
                month = 1;
                finalMonth = endMonth;
            }

            for (int i = month; i <= finalMonth; i++) {
                HashMap<Long, Double> monthVars = new HashMap<>();
                for (Variable variable : variables) {
                    Double monthValue = callBackend(String.format(variable.getLinkToData(), year, i));
                    monthVars.put(variable.getId(), monthValue);
                }

                Double value = calculateValueForMonth(indicator, monthVars);

                result.put(String.format(YEAR_PATTERN, year, String.format("%02d", i)), value);
            }
            year++;
        } while (year <= endYear);

        return result;
    }

    private Double calculateValueForMonth(Indicator indicator, HashMap<Long, Double> monthVars) {
        String formula = indicator.getFormula();
        for (Variable var : indicator.getVariables()) {
            formula = formula.replace(String.format(VAR_PATTERN, var.getId()), monthVars.get(var.getId()).toString());

        }

        ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");

        try {
            return (Double) engine.eval(formula);
        } catch (ScriptException e) {
            e.printStackTrace();
        }
        return 0D;
    }

    private Double callBackend(String url) {
        DefaultHttpClient httpClient = new DefaultHttpClient();
        HttpGet getRequest = new HttpGet(url);
        //getRequest.addHeader("accept", "application/json");

        try {
            HttpResponse response = httpClient.execute(getRequest);
            BufferedReader br = new BufferedReader(
                    new InputStreamReader((response.getEntity().getContent())));

            String output;
            while ((output = br.readLine()) != null) {
                return Double.valueOf(output);
            }

            httpClient.getConnectionManager().shutdown();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return 0D;

    }
}
