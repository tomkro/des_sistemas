package com.pucrs.ds.tf.application;

import com.pucrs.ds.tf.application.dto.LoggedDTO;
import com.pucrs.ds.tf.domain.User;
import org.springframework.stereotype.Component;

/**
 * Created by tkroth on 12/10/16.
*/
@Component
public interface UserService {

    User create(Long clientId, User user);

    User get(Long id);

    LoggedDTO authenticate(String username, String password, String ipAddress);

}
