package com.pucrs.ds.tf.application.dto;

/**
 * Created by tkroth on 27/11/16.
 */
public class LoggedDTO {
    public String sessionId;
    public Long clientId;
}
