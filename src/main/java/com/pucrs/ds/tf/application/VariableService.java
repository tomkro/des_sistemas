package com.pucrs.ds.tf.application;

import com.pucrs.ds.tf.domain.Variable;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by tkroth on 05/10/16.
 */
@Component
public interface VariableService {
    Variable create(Long clientId, Variable variable);

    Variable get(Long id);

    List<Variable> list(Long clientId);

}