package com.pucrs.ds.tf.application.impl;

import com.pucrs.ds.tf.application.ClientService;
import com.pucrs.ds.tf.application.UserCache;
import com.pucrs.ds.tf.application.UserService;
import com.pucrs.ds.tf.application.dto.LoggedDTO;
import com.pucrs.ds.tf.application.repository.UserRepository;
import com.pucrs.ds.tf.domain.Client;
import com.pucrs.ds.tf.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.persistence.EntityNotFoundException;
import java.security.InvalidParameterException;

/**
 * Created by tkroth on 12/10/16.
 */
@Component
public class UserServiceImpl implements UserService {

    @Autowired
    ClientService clientService;

    @Autowired
    UserRepository userRepository;

    @Override
    public User create(Long clientId, User user) {

        Client client = clientService.get(clientId);

        if (ObjectUtils.isEmpty(client)) {
            throw new InvalidParameterException("Cliente informado não existe");
        }


        user.setClient(client);
        user.setPassword(UserCache.getInstance().encrypt(user.getPassword()));

        return userRepository.save(user);
    }

    @Override
    public User get(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public LoggedDTO authenticate(String username, String password, String ipAddress) {
        String encryptedPassword = UserCache.getInstance().encrypt(password);

        User user = userRepository.findByEmailAndPassword(username, encryptedPassword);

        if(user == null) {
            throw new EntityNotFoundException("Usuário e/ou senha incorretos");
        }

        String sessionId = UserCache.getInstance().generateSessionId(user);

        String sessionKey = UserCache.getInstance().encrypt(sessionId.concat(".").concat(ipAddress));


        UserCache.getInstance().add(sessionKey, user);

        LoggedDTO loggedDTO = new LoggedDTO();
        loggedDTO.sessionId = sessionId;
        loggedDTO.clientId = user.getClient().getId();

        return loggedDTO;
    }


}
