package com.pucrs.ds.tf.application.impl;

import com.pucrs.ds.tf.application.ClientService;
import com.pucrs.ds.tf.application.repository.ClientRepository;
import com.pucrs.ds.tf.domain.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.security.InvalidParameterException;

/**
 * Created by tkroth on 05/10/16.
 */
@Component
public class ClientServiceImpl implements ClientService {

    @Autowired
    ClientRepository clientRepository;

    @Override
    public Client create(Client client) {
        if (StringUtils.isEmpty(client.getName())) {
            throw new InvalidParameterException("Informe um nome para o cliente");
        }
        if (client.getId() != null) {
            throw new UnsupportedOperationException("Para atualizar um cliente use o serviço adequado");
        }

        return clientRepository.save(client);
    }

    @Override
    public Client get(Long id) {
        return clientRepository.findOne(id);
    }
}
