package com.pucrs.ds.tf.application;

import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.pucrs.ds.tf.domain.User;

import java.io.UnsupportedEncodingException;
import java.security.InvalidParameterException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by tkroth on 13/10/16.
 */
public class UserCache {

    private HashMap<String, User> cache;

    private static UserCache userCache;

    public static UserCache getInstance() {
        if (userCache == null) {
            userCache = new UserCache();
        }
        return userCache;
    }

    public UserCache() {
        cache = new HashMap<>();
    }

    public void add(String sessionId, User user) {
        if (cache.containsKey(sessionId)) {
            throw new InvalidParameterException("Session already exists");
        }

        cache.put(sessionId, user);
    }

    public User get(String sessionId) {
        User user = cache.get(sessionId);
        return user;
    }

    public void remove(String sessionId) {
        cache.remove(sessionId);
    }

    public String encrypt(String password) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes("UTF-8"));
        }
        catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            throw new InvalidParameterException("Unknown error on password procedure");
        }

        byte[] digest = md.digest();
        return String.format("%064x", new java.math.BigInteger(1, digest));
    }

    public String generateSessionId(User user) {
        ObjectIdGenerators.UUIDGenerator uuidGenerator = new ObjectIdGenerators.UUIDGenerator();
        UUID uuid = uuidGenerator.generateId(user);
        return uuid.toString();
    }


}
