package com.pucrs.ds.tf.application.repository;

import com.pucrs.ds.tf.domain.Client;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by tkroth on 05/10/16.
 */
public interface ClientRepository extends CrudRepository<Client, Long> {
}
