package com.pucrs.ds.tf.application.dto;

/**
 * Created by tkroth on 17/10/16.
 */
public class LoginDTO {
    public String username;
    public String password;

    public LoginDTO() {

    }
}
