package com.pucrs.ds.tf.application.repository;

import com.pucrs.ds.tf.domain.Indicator;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

/**
 * Created by tkroth on 18/10/16.
 */
public interface IndicatorRepository extends CrudRepository<Indicator, Long> {

	List<Indicator> findByClientId(Long clientId);
	
}
