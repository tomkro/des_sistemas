package com.pucrs.ds.tf.infra.controller;

import com.pucrs.ds.tf.application.ClientService;
import com.pucrs.ds.tf.domain.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by tkroth on 05/10/16.
 */
@RestController
@RequestMapping(value = "/client")
public class ClientRestController {

    @Autowired
    ClientService clientService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public Client create(@RequestBody final Client client) {
        return clientService.create(client);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Client get(@PathVariable(value = "id") final Long id) {
        return clientService.get(id);
    }

}
