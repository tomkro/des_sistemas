package com.pucrs.ds.tf.infra.controller;

import com.pucrs.ds.tf.application.UserService;
import com.pucrs.ds.tf.application.dto.LoggedDTO;
import com.pucrs.ds.tf.application.dto.LoginDTO;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by tkroth on 17/10/16.
 */
@RestController
@RequestMapping(value = "/")
public class AuthenticationRestController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "authenticate", method = RequestMethod.POST)
    public LoggedDTO authenticate (@RequestBody final LoginDTO loginDTO, HttpServletRequest httpServletRequest) {
        LoggedDTO authenticate = userService.authenticate(loginDTO.username, loginDTO.password, httpServletRequest.getRemoteAddr());
        return authenticate;
    }


}
