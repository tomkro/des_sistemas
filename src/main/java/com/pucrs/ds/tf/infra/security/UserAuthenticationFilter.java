package com.pucrs.ds.tf.infra.security;

import com.pucrs.ds.tf.application.UserCache;
import com.pucrs.ds.tf.domain.User;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by tkroth on 13/10/16.
 */
public class UserAuthenticationFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String sessionId = request.getHeader("sessionId");

        /*if(StringUtils.isEmpty(sessionId)) {
            throw new SecurityException("SessionId is not present as a Header on the request");
        }

        String remoteAddr = request.getRemoteAddr();

        String sessionKey = sessionId.concat(".").concat(remoteAddr);
        String encryptedSessionKey = UserCache.getInstance().encrypt(sessionKey);

        User user = UserCache.getInstance().get(encryptedSessionKey);

        if(user == null) {
            throw new SecurityException("User not atuthenticated");
        }
*/
        filterChain.doFilter(request, response);

    }
}
