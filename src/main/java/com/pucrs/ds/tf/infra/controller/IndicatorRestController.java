package com.pucrs.ds.tf.infra.controller;

import com.pucrs.ds.tf.application.IndicatorService;
import com.pucrs.ds.tf.domain.Indicator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.List;

/**
 * Created by tkroth on 05/10/16.
 */
@RestController
@RequestMapping(value = "/client/{client_id}/indicator")
public class IndicatorRestController {

    @Autowired
    IndicatorService indicatorService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public Indicator create(@PathVariable(value = "client_id") final Long clientId, @RequestBody final Indicator indicator) {
        return indicatorService.create(clientId, indicator);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Indicator get(@PathVariable(value = "id") final Long id) {
        return indicatorService.get(id);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Indicator> list(@PathVariable(name = "client_id") final Long clientId) {
        return indicatorService.list(clientId);
    }

    @RequestMapping(value = "/{id}/begin/{iniYear}/{iniMonth}/end/{endYear}/{endMonth}", method = RequestMethod.GET)
    public Map<String, Double> processIndicator(@PathVariable(value = "id") final Long id,
                                                @PathVariable(value = "iniYear") final int iniYear,
                                                @PathVariable(value = "iniMonth") final int iniMonth,
                                                @PathVariable(value = "endYear") final int endYear,
                                                @PathVariable(value = "endMonth") final int endMonth) {
        return indicatorService.processIndicator(id, iniYear, iniMonth, endYear, endMonth);
    }

}
