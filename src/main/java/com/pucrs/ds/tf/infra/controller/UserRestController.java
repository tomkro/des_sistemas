package com.pucrs.ds.tf.infra.controller;

import com.pucrs.ds.tf.application.UserService;
import com.pucrs.ds.tf.domain.Client;
import com.pucrs.ds.tf.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

/**
 * Created by tkroth on 05/10/16.
 */
@RestController
@RequestMapping(value = "/client/{client_id}/user")
public class UserRestController {

    @Autowired
    UserService userService;
    
    @RequestMapping(value = "", method = RequestMethod.POST)
    public User create(@PathVariable(value = "client_id") final Long clientId, @RequestBody final User user) {
        return userService.create(clientId, user);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public User get(@PathVariable(value = "id") final Long id) {
        return userService.get(id);
    }

}
