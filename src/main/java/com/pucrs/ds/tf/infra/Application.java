package com.pucrs.ds.tf.infra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Tomas on 16/09/2016.
 */

@RestController
@SpringBootApplication(scanBasePackages = "com.pucrs.ds.tf")
@EnableJpaRepositories(basePackages = "com.pucrs.ds.tf.application.repository")
@EntityScan(basePackages = "com.pucrs.ds.tf.domain")
public class Application {

    @RequestMapping("/")
    public String healthCheck() {
        return "Yep, I'm alive! Trabalho de desenvolvimento de sistemas 2016-2!";
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }
}
