package com.pucrs.ds.tf.infra.controller;

import com.pucrs.ds.tf.application.VariableService;
import com.pucrs.ds.tf.domain.Variable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by tkroth on 05/10/16.
 */
@RestController
@RequestMapping(value = "/client/{id}/variable")
public class VariableRestController {

    @Autowired
    VariableService variableService;

    @RequestMapping(value = "", method = RequestMethod.POST)
    public Variable create(@PathVariable(name = "id") final Long clientId, @RequestBody final Variable variable) {
        return variableService.create(clientId, variable);
    }

    
    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Variable> list(@PathVariable(name = "id") final Long clientId) {
        return variableService.list(clientId);
    }

    @RequestMapping(value = "/{variableId}", method = RequestMethod.GET)
    public Variable get(@PathVariable(name = "variableId") final Long variableId) {
        return variableService.get(variableId);
    }

}
