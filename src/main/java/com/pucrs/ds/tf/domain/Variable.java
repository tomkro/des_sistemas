package com.pucrs.ds.tf.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by tkroth on 05/10/16.
 */
@Entity
@Table(name = "variables")
public class Variable {

    @Id
    @Column(name = "variable_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;

    @NotNull
    private String name;

    private String unit;

    private String linkToData;

    public Variable() {

    }


    public Long getId() {
        return id;
    }

    public Client getClient() {
        return client;
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    public String getLinkToData() {
        return linkToData;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
