package com.pucrs.ds.tf.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by tkroth on 05/10/16.
 */
@Entity
@Table(name = "clients")
public class Client {

    @Id
    @Column(name = "client_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(unique = true)
    private String name;


    private Client() {

    }

    public Client(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
