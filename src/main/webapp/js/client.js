// Setup login form
$(".form-signin").submit(function(event) {
    console.log(event);
    $(".form-signin .form-message").removeClass("has-error");
    $.ajax({
      url: "authenticate",
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify({"username": event.target.elements.username.value,
                            "password": event.target.elements.password.value}),
      dataType: "json"
    }).fail(function( data ) {
      if (console && console.log ) {
        console.log("Login failure -", data);
      }
      var msg;
      try {
        var json = $.parseJSON(data.responseText);
        msg = json.message;
      } catch(err) {
        msg = "Erro inesperado.";
        console.log(err);
      }
      /*if ("responseJSON" in data) {
        msg = data.responseJSON.message;
      } else {
        msg = "Erro inesperado.";
      }*/
      $(".form-signin .form-message .help-block").text(msg);
      $(".form-signin .form-message").addClass("has-error");
    })
      .done(function( data ) {
        if ( console && console.log ) {
          console.log( "Data:", data );
        }
        window.localStorage.setItem('sessionId', data.sessionId);
        window.localStorage.setItem('clientId', data.clientId);
        document.location = "app.html";
      });
      event.preventDefault();
});

//